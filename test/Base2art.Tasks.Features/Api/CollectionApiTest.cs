﻿
namespace Base2art.Tasks.Api
{
    using System;
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using NUnit.Framework;
    
    [TestFixture]
    public class CollectionApiTest
    {
        [Test]
        public async void Select_Async()
        {
            string[] data = await SampleTasks.Coll()
                .Then()
                .Select(x => Task.FromResult("a" + x))
                .Then()
                .ToArray();
        }
        
        [Test]
        public async void Select()
        {
            string[] data = await SampleTasks.Coll()
                .Then()
                .Select(x => "a" + x)
                .Then()
                .ToArray();
        }

        [Test]
        public async void FirstOrDefault()
        {
            await SampleTasks.Coll()
                .Then()
                .FirstOrDefault();
        }

        [Test]
        public async void Where_Async()
        {
            await SampleTasks.Coll()
                .Then()
                .Where(x => Task.FromResult(x == "A"))
                .Then()
                .ToArray();
        }

        [Test]
        public async void Where()
        {
            await SampleTasks.Coll()
                .Then()
                .Where(x => x == "A")
                .Then()
                .ToArray();
        }
        
        [Test]
        public async void ToArray()
        {
            string[] data = await SampleTasks.Coll()
                .Then()
                .Select(x => Task.FromResult("a" + x))
                .Then()
                .ToArray();
        }
        
        [Test]
        public async void ToList()
        {
            List<string> data = await SampleTasks.Coll()
                .Then()
                .Select(x => "a" + x)
                .Then()
                .ToList();
        }
    }
}


