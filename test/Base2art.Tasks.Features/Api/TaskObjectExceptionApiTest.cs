﻿
using System;
using System.Diagnostics;
using System.Threading.Tasks;
using NUnit.Framework;
namespace Base2art.Tasks.Api
{
    [TestFixture]
    public class TaskObjectExceptionApiTest
    {
        [Test]
        public async void When_Exception_Action()
        {
            Func<Task<string>> aProducer = SampleTasks.A;
            
            await aProducer.When()
                .Exception((x) => "B")
                .Success((a) => "C")
                .Execute();
        }
        
        [Test]
        public async void When_Exception_Async()
        {
            Func<Task<string>> aProducer = SampleTasks.A;
            
            await aProducer.When()
                .Exception(async (x) => await SampleTasks.B())
                .Success(async (a) =>  await SampleTasks.B())
                .Execute();
        }
    }
}


