﻿
using System;
using System.Diagnostics;
using System.Threading.Tasks;
using NUnit.Framework;

namespace Base2art.Tasks.Api
{
    [TestFixture]
    public class TaskExceptionApiTest
    {
        [Test]
        public async void When_Exception_Action()
        {
            Func<Task> aProducer = SampleTasks.A;
            
            await aProducer.When()
                .Exception((x) => Debug.WriteLine(x))
                .Success(() => Console.WriteLine("Done"))
                .Execute();
        }
        
        [Test]
        public async void When_Exception_Async()
        {
            Func<Task> aProducer = SampleTasks.A;
            
            await aProducer.When()
                .Exception(async (x) => await Task.Factory.StartNew(() => Debug.WriteLine(x)))
                .Success(async () => await Task.Factory.StartNew(() => Console.WriteLine("Done")))
                .Execute();
        }
    }
    
    
}
