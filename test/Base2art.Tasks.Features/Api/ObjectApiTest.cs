﻿
using System;
using System.Threading.Tasks;
using NUnit.Framework;
namespace Base2art.Tasks.Api
{
    [TestFixture]
    public class ObjectApiTest
    {
        [Test]
        public async void Return_Async()
        {
            await SampleTasks.A()
                .Then()
                .Return((a) => SampleTasks.B());
        }
        
        [Test]
        public async void Return_Action()
        {
            await SampleTasks.A()
                .Then()
                .Return((a) => "B");
        }
        
        [Test]
        public async void Coalesce_Async()
        {
            await SampleTasks.Null()
                .Then()
                .Coalesce(() => SampleTasks.B());
        }
        
        [Test]
        public async void Coalesce_Action()
        {
            await SampleTasks.Null()
                .Then()
                .Coalesce(() => "B");
        }
        
        [Test]
        public async void Coalesce_Value()
        {
            await SampleTasks.Null()
                .Then()
                .Coalesce("B");
        }
        
        
        [Test]
        public async void Execute_Async()
        {
            await SampleTasks.A()
                .Then()
                .Execute(async () => await SampleTasks.LogAsync());
        }
        
        [Test]
        public async void Execute_Action()
        {
            await SampleTasks.A()
                .Then()
                .Execute(() => SampleTasks.Log());
        }
    }
}


