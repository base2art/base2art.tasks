﻿
using System;
using System.Threading.Tasks;
using NUnit.Framework;

namespace Base2art.Tasks.Api
{
    [TestFixture]
    public class TaskApiTest
    {
        [Test]
        public async void Return_Async()
        {
            await SampleTasks.A()
                .Then()
                .Return(SampleTasks.B);
        }
        
        [Test]
        public async void Return_Action()
        {
            await SampleTasks.A()
                .Then()
                .Return(() => "B");
        }
        
        [Test]
        public async void Return_Value()
        {
            await SampleTasks.A()
                .Then()
                .Return("B");
        }
        
        [Test]
        public async void Execute_Async()
        {
            await SampleTasks.A()
                .Then()
                .Execute(async () => await SampleTasks.LogAsync());
        }
        
        [Test]
        public async void Execute_Action()
        {
            await SampleTasks.A()
                .Then()
                .Execute(() => SampleTasks.Log());
        }
    }
    
    
}
