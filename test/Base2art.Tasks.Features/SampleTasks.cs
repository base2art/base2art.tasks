﻿
using System;
using System.Threading.Tasks;

namespace Base2art.Tasks
{
    public static class SampleTasks
    {

        public static Task<string> A()
        {
            return Task.FromResult("A");
        }
        
        public static Task<string> B()
        {
            return Task.FromResult("B");
        }
        
        public static Task<string> Null()
        {
            return Task.FromResult<string>(null);
        }
        
        public static Task<string[]> Coll()
        {
            return Task.FromResult(new string[]{ "A", "B" });
        }
        
        public static void Log()
        {
            return;
        }
        
        public static Task LogAsync()
        {
            return Task.FromResult(true);
        }
    }
}
