﻿namespace Base2art.Tasks.Assertions
{
    using System;
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using NUnit.Framework;
    using FluentAssertions;

    [TestFixture]
    public class ThenTester_Select : ThenTesterBase
    {
        [Test]
        public async void ShouldDoListNext_Select_NonNull()
        {
            var rez = await(this.ListAsync(1).Then().Select(x => x.Name)).Then().ToArray();
            rez[0].Should().Be("0");
            var rez1 = await(this.ListAsync(1).Then().Select(x => x.Name)).Then().ToList();
            rez1[0].Should().Be("0");
        }

        [Test]
        public async void ShouldDoListNext_Select_Null()
        {
            var rez = await(this.ListAsync(null).Then().Select(x => x.Name));
            rez.Should().BeNull();
        }

        [Test]
        public async void ShouldDoArrayNext_Select_NonNull()
        {
            var rez = await(this.ArrayAsync(1).Then().Select(x => x.Name)).Then().ToArray();
            rez[0].Should().Be("0");
        }

        [Test]
        public async void ShouldDoArrayNext_Select_Null()
        {
            var rez = await(this.ArrayAsync(null).Then().Select(x => x.Name));
            rez.Should().BeNull();
        }

        [Test]
        public async void ShouldDoArrayNext_Select_Null_async()
        {
            var rez = await(this.ArrayAsync(null).Then().Select(x => Task.FromResult(x.Name)));
            rez.Should().BeNull();
        }
    }
}


