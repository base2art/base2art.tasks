﻿namespace Base2art.Tasks.Assertions
{
    using System;
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using NUnit.Framework;
    using FluentAssertions;

    [TestFixture]
    public class ThenTester_Where : ThenTesterBase
    {
        [Test]
        public async void ShouldDo_Where_NonTask_Null()
        {
            var rez = await (this.ArrayAsync(null).Then().Where(x => x.Name == "1"));
            rez.Should().BeNull();
        }
        
        [Test]
        public async void ShouldDo_Where_NonTask_Null_async()
        {
            var rez = await (this.ArrayAsync(null).Then().Where(x => Task.FromResult(x.Name == "1")));
            rez.Should().BeNull();
        }

        [Test]
        public async void ShouldDo_Where_NonTask_NonNull()
        {
            (await this.ArrayAsync(1).Then().Where(x => x.Name == "1").Then().FirstOrDefault()).Should().BeNull();
            (await this.ArrayAsync(2).Then().Where(x => x.Name == "1").Then().FirstOrDefault()).Name.Should().Be("1");
        }

        [Test]
        public async void ShouldDo_Where_NonTask_NonNull_async()
        {
            (await this.ArrayAsync(1).Then().Where(x => Task.FromResult(x.Name == "1")).Then().FirstOrDefault()).Should().BeNull();
            (await this.ArrayAsync(2).Then().Where(x => Task.FromResult(x.Name == "1")).Then().FirstOrDefault()).Name.Should().Be("1");
        }
//
//        [Test]
//        public async void ShouldDo_Where_Task_Null()
//        {
//            var rez = await (this.ArrayAsync(null).Then().Select(x => x.Name == "1"));
//            rez.Should().BeNull();
//        }

//        [Test]
//        public async void ShouldDo_Where_Task_NonNull()
//        {
//            (await this.ArrayAsync(2).Then().Where(async x => x.Name == "2").Then().FirstOrDefault()).Name.Should().Be("2");
//        }
    }
}


