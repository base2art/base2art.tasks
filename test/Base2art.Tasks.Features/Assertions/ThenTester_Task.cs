﻿namespace Base2art.Tasks.Assertions
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Threading.Tasks;
    using NUnit.Framework;
    using FluentAssertions;

    [TestFixture]
    public class ThenTester_Task : ThenTesterBase
    {
        [Test]
        public async void Should_Get()
        {
            var task = this.ListAsync(1).Then().Return(1);
            this.List.Should().BeNull();
            var data = await task;
            this.List.Should().NotBeNull();
            data.Should().Be(1);
        }

        [Test]
        public async void Should_Get_delegate()
        {
            var task = this.ListAsync(1).Then().Return(() => 2);
            this.List.Should().BeNull();
            var data = await task;
            this.List.Should().NotBeNull();
            data.Should().Be(2);
        }

        [Test]
        public async void Should_Get_delegateAsync()
        {
            var task = this.ListAsync(1).Then().Return(async () => 2);
            this.List.Should().BeNull();
            var data = await task;
            this.List.Should().NotBeNull();
            data.Should().Be(2);
        }
        
        [Test]
        public async void Should_Get_Exception()
        {
            int count = 0;
            Func<Task> item = async () =>
            {
                count += 1;
                if (count < 3)
                {
                    throw new Exception();
                }
            };

            var task = item.When().ExecuteWithRetry(times: 4);

            await task;
            count.Should().Be(3);
        }
        
        [Test]
        public async void Should_Get_Exception_overrun()
        {
            int count = 0;
            Func<Task> item = async () =>
            {
                count += 1;
                if (count < 6)
                {
                    throw new Exception("abc");
                }
            };

            var task = item.When().ExecuteWithRetry(times: 4);

            try
            {
                await task;
                Assert.Fail();
            }
            catch (AssertionException)
            {
                throw;
            }
            catch (Exception ex)
            {
                ex.Message.Should().Be("abc");
            }

            count.Should().Be(5);
        }

        [Test]
        public async void Should_Get_Final_int()
        {
            int count = 0;
            Func<Task<int>> item = () =>
            {
                count += 1;
                if (count < 4)
                {
                    throw new Exception("abc");
                }

                return Task.FromResult(count);
            };

            var task = item.When().ExecuteWithRetry(times: 3);

            var rez = await task;

            rez.Should().Be(4);
            count.Should().Be(4);
        }

        [Test]
        public async void Should_Get_Final()
        {
            int count = 0;
            Func<Task> item = () =>
            {
                count += 1;
                if (count < 4)
                {
                    throw new Exception("abc");
                }

                return Task.FromResult(count);
            };

            var task = item.When().ExecuteWithRetry(times: 3);
            await task;

            count.Should().Be(4);
        }
        
        [Test]
        public async void Should_Execute()
        {
            int count = 0;
            Func<Task> item = async () =>
            {
                count += 1;
            };
            
            Func<Task> Log = async () =>
            {
                count += 1;
            };
            
            var task = item().Then().Execute(Log);
            
            await task;
            
            count.Should().Be(2);
        }
        
        [Test]
        public async void Should_Execute_NonAsync()
        {
            int count = 0;
            Func<Task> item = async () =>
            {
                count += 1;
            };
            
            Action Log = () =>
            {
                count += 1;
            };
            
            var task = item().Then().Execute(Log);
            
            await task;
            
            count.Should().Be(2);
        }
        
        [Test]
        public async void Should_Execute_Exception()
        {
            int count = 0;
            Func<Task> item = async () =>
            {
                count += 1;
                if (count < 3)
                {
                    throw new Exception();
                }
            };
            
            Func<Exception, Task> log1 = async (x) =>
            {
                count += 1;
            };
            
            Action<Exception> log2 = (x) =>
            {
                count += 1;
            };
            
            var task = item
                .When()
                .Exception(log1)
                .Exception(log2)
                .Success(() => { })
                .ExecuteWithRetry(times: 1);
            try
            {
                await task;
            }
            catch (Exception)
            {
                count.Should().Be(4);
                
                return;
            }
            
            Assert.Fail();
        }
        
        [Test]
        public async void TestWrap_Exceptions_Thrown_Retry()
        {
            Func<Task> task = () => Task.Factory.StartNew(()=> Console.WriteLine("STARTED"));
            
            await task.When()
                .Exception(ex => Debug.WriteLine(ex))
                .Success(() => Console.WriteLine("done"))
                .ExecuteWithRetry(times: 2);
            
        }
        
        [Test]
        public async void TestWrap_Exceptions_Thrown_Retry_async()
        {
            Func<Task> task = () => Task.Factory.StartNew(()=> Console.WriteLine("STARTED"));
            
            await task.When()
                .Success(() => Task.Factory.StartNew(()=> Console.WriteLine("STARTED")))
                .Exception(ex => Task.Factory.StartNew(()=> Debug.WriteLine("STARTED")))
                .ExecuteWithRetry(times: 2);
        }
    }
}




