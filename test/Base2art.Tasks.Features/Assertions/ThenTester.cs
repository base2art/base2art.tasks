﻿namespace Base2art.Tasks
{
    using System;
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using NUnit.Framework;
    
    public class ThenTesterBase
    {
        List<Person> list;

        public List<Person> List
        {
            
            get { return list; }
        }
        
        [SetUp]
        public void BeforeEach()
        {
            this.list = null;
        }
        
        public async Task<List<Person>> ListAsync(int? count)
        {
            if (!count.HasValue)
            {
                return null;
            }
            
            var list = new List<Person>();
            for (int i = 0; i < count.Value; i++)
            {
                list.Add(new Person{ Name = "" + i });
            }
            
            await Task.Delay(TimeSpan.FromMilliseconds(50));
            this.list = list;
            return list;
        }

        public async Task<Person[]> ArrayAsync(int? count)
        {
            //            this.ListAsync().MapAsync(x=>x);
            var data = await this.ListAsync(count);
            if (data == null)
            {
                return null;
            }
            
            return data.ToArray();
        }

        protected async Task<int> MapAsync(int i, Func<int, int> ret)
        {
            return ret(i);
        }
        
        public class Person
        {
            public string Name { get; set; }
        }
    }
}
