﻿namespace Base2art.Tasks.Assertions
{
    using System;
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using NUnit.Framework;
    using FluentAssertions;

    [TestFixture]
    public class ThenTester_Object_Select : ThenTesterBase
    {
        [Test]
        public async void TestWrap_Basic()
        {
            Task task = Task.FromResult("abc");
            var result = await task.Then().Return(() => 4);
            result.Should().Be(4);
        }
        
        [Test]
        public async void TestWrap_Exceptions_NoneThrown()
        {
            Func<Task<string>> task = () => this.DataAbc();
            var result = await task.When().Success((a) => Data123()).Execute();
            result.Should().Be("123");
            
            Func<Task<string>> task1 = () => this.DataAbc();
            var result1 = await task1.When().Exception((x) => Data123()).Execute();
            result1.Should().Be("abc");
        }
        
        [Test]
        public async void TestWrap_Exceptions_NoneThrown_Log()
        {
            Func<Task<string>> task = () => this.DataAbc();
            Action<string> log = Console.WriteLine;
            var result = await task.When().Exception((a) => "sdf").Execute();
            result.Should().Be("abc");
            
            Func<Task<string>> task1 = () => this.DataAbc();
            var result1 = await task1.When().Exception((x) => "sdf").Execute();
            result1.Should().Be("abc");
        }
        
        [Test]
        public async void TestWrap_Exceptions_Thrown()
        {
            Func<Task<string>> task = async () =>
            {
                throw new Exception("");
                return await this.DataAbc();
            };
            var result = await task.When().Exception((ex) => this.Data123()).Execute();
            result.Should().Be("123");
            
            Func<Task<string>> task1 = async () =>
            {
                throw new Exception("");
                return await this.DataAbc();
            };
            var result1 = await task1.When().Exception((ex) => "123").Execute();
            result1.Should().Be("123");
        }
        
        [Test]
        public async void TestWrap_Exceptions_Thrown_Retry()
        {
            Func<Task<string>> task = async () => await this.DataAbc();
            
            string result = await task.When()
                .Exception(ex => this.Data123())
                .Success((pre) => pre + "Done")
                .ExecuteWithRetry(times: 2);
            
            result.Should().Be("abcDone");
        }
        
        [Test]
        public async void TestWrap_Exceptions_Thrown_Retry_async()
        {
            Func<Task<string>> task = async () => await this.DataAbc();
            
            string result = await task.When()
                .Success((pre) => Task.FromResult(pre + "Done"))
                .Exception(ex => "asd")
                .ExecuteWithRetry(times: 2);
            
            result.Should().Be("abcDone");
        }
        
        [Test]
        public async void Should_Get_Exception()
        {
            int count = 0;
            Func<Task<int>> item = () =>
            {
                count += 1;
                if (count < 3)
                {
                    throw new Exception();
                }
                
                return Task.FromResult(count);
            };
            
            var task = item.When().ExecuteWithRetry(times: 4);
            
            var counti = await task;
            count.Should().Be(3);
            counti.Should().Be(3);
        }
        
        [Test]
        public async void Should_Get_Exception_overrun()
        {
            int count = 0;
            Func<Task<int>> item = () =>
            {
                count += 1;
                if (count < 6)
                {
                    throw new Exception("abc");
                }
                
                return Task.FromResult(count);
            };
            
            var task = item.When().ExecuteWithRetry(times: 4);
            
            try
            {
                await task;
                Assert.Fail();
            }
            catch (AssertionException ae)
            {
                throw;
            }
            catch (Exception ex)
            {
                ex.Message.Should().Be("abc");
            }
            
            count.Should().Be(5);
        }
        
        [Test]
        public async void TestWrap()
        {
            var result = await Task.FromResult("abc").Then().Return(async x => x.Length).Then().Return(x => x * 2);
            result.Should().Be(6);
            var result1 = await Task.FromResult(true).Then().Return(async x => !x);
            result1.Should().BeFalse();
        }
        
        [Test]
        public async void Should_Get()
        {
            var task = this.ListAsync(1).Then().FirstOrDefault().Then().Return(x => x.Name + "1");
            this.List.Should().BeNull();
            var data = await task;
            data.Should().Be("01");
        }
        
        [Test]
        public async void Should_Get_Async()
        {
            var task = this.ListAsync(1).Then().FirstOrDefault().Then().Return(async x => x.Name + "1");
            this.List.Should().BeNull();
            var data = await task;
            data.Should().Be("01");
        }
        
        [Test]
        public async void Should_Get_Null()
        {
            var data = await this.ListAsync(1).Then().FirstOrDefault().Then().Return<string>(x => (string)null).Then().Return(x => x.Trim());
            data.Should().Be(null);
        }
        
        [Test]
        public async void Should_Get_Async_Null()
        {
            var data = await this.ListAsync(1).Then().FirstOrDefault().Then().Return<string>(async x => (string)null).Then().Return<string>(async x => x.Trim());
            data.Should().Be(null);
        }
        
        [Test]
        public async void Should_CoalesceSimple()
        {
            var data = await this.ListAsync(1)
                .Then()
                .FirstOrDefault()
                .Then()
                .Return<string>(async x => (string)null)
                .Then()
                .Coalesce(" ")
                .Then()
                .Return<string>(async x => x.Trim());
            data.Should().Be("");
        }
        
        [Test]
        public async void Should_CoalescefallThrough()
        {
            var data = await this.ListAsync(1)
                .Then()
                .FirstOrDefault()
                .Then()
                .Return<string>(async x => " abc ")
                .Then()
                .Coalesce(() => " ")
                .Then()
                .Return<string>(async x => x.Trim());
            data.Should().Be("abc");
        }
        
        [Test]
        public async void Should_CoalescefallThrough_Async()
        {
            var data = await this.ListAsync(1)
                .Then()
                .FirstOrDefault()
                .Then()
                .Return<string>(async x => " abc ")
                .Then()
                .Coalesce(async () => " ")
                .Then()
                .Return<string>(async x => x.Trim());
            data.Should().Be("abc");
        }
        
        [Test]
        public async void Should_Coalesce()
        {
            var data = await this.ListAsync(1)
                .Then()
                .FirstOrDefault()
                .Then()
                .Return<string>(async x => (string)null)
                .Then()
                .Coalesce(() => " ")
                .Then()
                .Return<string>(async x => x.Trim());
            data.Should().Be("");
        }
        
        [Test]
        public async void Should_Coalesce_Async()
        {
            var data = await this.ListAsync(1)
                .Then()
                .FirstOrDefault()
                .Then()
                .Return<string>(async x => (string)null)
                .Then()
                .Coalesce(async () => " ")
                .Then()
                .Return<string>(async x => x.Trim());
            data.Should().Be("");
        }

        private Task<string> DataAbc()
        {
            return Task.FromResult("abc");
        }

        private Task<string> Data123()
        {
            return Task.FromResult("123");
        }
    }
}


