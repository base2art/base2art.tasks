﻿
namespace Base2art.Tasks
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;

    public class CollectionThenner<TColl, TElement> : ObjectThenner<TColl>
        where TColl : class, IEnumerable<TElement>
    {
        public CollectionThenner(Task<TColl> task)
            : base(task)
        {
        }

        public Task<TElement[]> ToArray()
        {
            return base.Return(x => x == null ? null : x.ToArray());
        }

        public Task<List<TElement>> ToList()
        {
            return base.Return(x => x == null ? null : x.ToList());
        }

        public Task<TElement> FirstOrDefault()
        {
            return base.Return(x => x == null ? default(TElement) : x.FirstOrDefault());
        }

        public async Task<IEnumerable<TResult>> Select<TResult>(Func<TElement, TResult> map)
        {
            var rez = await this.Task;
            
            return rez == null ? null : rez.Select(map).ToArray();
        }

        public async Task<IEnumerable<TResult>> Select<TResult>(Func<TElement, Task<TResult>> map)
        {
            var rez = await this.Task;
            if (rez == null)
            {
                return null;
            }
            
            return (await (System.Threading.Tasks.Task.WhenAll(rez.Select(map))));
        }

        public async Task<IEnumerable<TElement>> Where(Func<TElement, bool> map)
        {
            var rez = await this.Task;
            if (rez == null)
            {
                return null;
            }
            
            return rez.Where(map);
        }

        public async Task<IEnumerable<TElement>> Where(Func<TElement, Task<bool>> map)
        {
            var rez = await this.Task;
            if (rez == null)
            {
                return null;
            }
            
            List<TElement> items = new List<TElement>();
            foreach (var item in rez)
            {
                if (await map(item))
                {
                    items.Add(item);
                }
            }
            
            return items;
        }
    }
}


