﻿
namespace Base2art.Tasks
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;

    public static class Thenner
    {
        public static TaskThenner Then(this Task input)
        {
            return new TaskThenner(input);
        }
        
        public static ObjectThenner<TIn> Then<TIn>(this Task<TIn> input)
        {
            return new ObjectThenner<TIn>(input);
        }

        public static CollectionThenner<List<TIn>, TIn> Then<TIn>(this Task<List<TIn>> input)
        {
            return new CollectionThenner<List<TIn>, TIn>(input);
        }

        public static CollectionThenner<TIn[], TIn> Then<TIn>(this Task<TIn[]> input)
        {
            return new CollectionThenner<TIn[], TIn>(input);
        }

        public static CollectionThenner<IEnumerable<TIn>, TIn> Then<TIn>(this Task<IEnumerable<TIn>> input)
        {
            return new CollectionThenner<IEnumerable<TIn>, TIn>(input);
        }
        
        public static ExceptionalFuncTaskThenner When(this Func<Task> input)
        {
            return new ExceptionalFuncTaskThenner(input);
        }
        
        public static ExceptionalFuncObjectThenner<TIn> When<TIn>(this Func<Task<TIn>> input)
        {
            return new ExceptionalFuncObjectThenner<TIn>(input);
        }
    }
}


