﻿
namespace Base2art.Tasks
{
    using System;
    using System.Linq;
    using System.Threading.Tasks;

    public class ObjectThenner<TIn> : TaskThenner
    {
        private readonly Task<TIn> task;

        public ObjectThenner(Task<TIn> task)
            : base(task)
        {
            this.task = task;
        }

        protected new Task<TIn> Task
        {
            get
            {
                return task;
            }
        }
        
        public async Task<TResult> Return<TResult>(Func<TIn, TResult> map)
        {
            var rez = await this.Task;
            if (rez == null)
            {
                return default(TResult);
            }
            
            return map(rez);
        }
        
        public async Task<TResult> Return<TResult>(Func<TIn, Task<TResult>> map)
        {
            var rez = await this.Task;
            if (rez == null)
            {
                return default(TResult);
            }
            
            return await map(rez);
        }
        
        public Task<TIn> Coalesce(TIn result)
        {
            return this.Coalesce(() => result);
        }
        
        public async Task<TIn> Coalesce(Func<TIn> map)
        {
            var rez = await this.Task;
            if (rez == null)
            {
                return map();
            }
            
            return rez;
        }
        
        public async Task<TIn> Coalesce(Func<Task<TIn>> map)
        {
            var rez = await this.Task;
            if (rez == null)
            {
                return await map();
            }
            
            return rez;
        }
    }
}


