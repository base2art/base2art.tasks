﻿
namespace Base2art.Tasks
{
    using System;
    using System.Linq;
    using System.Threading.Tasks;

    public class ExceptionalFuncObjectThenner<TIn> : ExceptionalFuncTaskThenner
    {
        private readonly Func<Task<TIn>> task;
        private Func<Exception, Task<TIn>> exceptionRunner;
        private Func<TIn, Task<TIn>> successRunner;
        private Func<Exception, TIn> exceptionRunnerAction;
        private Func<TIn, TIn> successRunnerAction;

        
        public ExceptionalFuncObjectThenner(Func<Task<TIn>> task)
            : base(task)
        {
            this.task = task;
        }

        protected new Func<Task<TIn>> Task
        {
            get
            {
                return task;
            }
        }
        
        
        public ExceptionalFuncObjectThenner<TIn> Exception(Func<Exception, Task<TIn>> exceptionRunner)
        {
            this.exceptionRunner = exceptionRunner;
            return this;
        }
        
        public ExceptionalFuncObjectThenner<TIn> Success(Func<TIn, Task<TIn>> successRunner)
        {
            this.successRunner = successRunner;
            return this;
        }
        
        public ExceptionalFuncObjectThenner<TIn> Exception(Func<Exception, TIn> exceptionRunner)
        {
            this.exceptionRunnerAction = exceptionRunner;
            return this;
        }
        
        public ExceptionalFuncObjectThenner<TIn> Success(Func<TIn, TIn> successRunner)
        {
            this.successRunnerAction = successRunner;
            return this;
        }

        public new Task<TIn> Execute()
        {
            return this.Retry(0, 0);
        }

        public new Task<TIn> ExecuteWithRetry(int times)
        {
            return this.Retry(0, times);
        }

        private async Task<TIn> Retry(int current, int times)
        {
            if (current == times)
            {
                Exception exi = null;
                TIn data = default(TIn);
                try
                {
                    data = await this.Task();
                    
                    
                    if (this.successRunner != null)
                    {
                        data = await this.successRunner(data);
                    }
                    
                    if (this.successRunnerAction != null)
                    {
                        data = this.successRunnerAction(data);
                    }
                    
                    return data;
                }
                catch (Exception ex)
                {
                    exi = ex;
                }
                
                if (exi != null)
                {
                    if (this.exceptionRunner != null || this.exceptionRunnerAction != null)
                    {
                        if (this.exceptionRunner != null)
                        {
                            data = await this.exceptionRunner(exi);
                        }
                        
                        if (this.exceptionRunnerAction != null)
                        {
                            data = this.exceptionRunnerAction(exi);
                        }
                    }
                    else
                    {
                        throw exi;
                    }
                }
                
                return data;
            }
            else
            {
                try
                {
                    TIn data = await this.Task();
                    if (this.successRunner != null)
                    {
                        data = await this.successRunner(data);
                    }
                    
                    if (this.successRunnerAction != null)
                    {
                        data = this.successRunnerAction(data);
                    }
                    
                    return data;
                }
                catch (Exception)
                {
                }
                
                return await this.Retry(current + 1, times);
            }
        }
    }
}






