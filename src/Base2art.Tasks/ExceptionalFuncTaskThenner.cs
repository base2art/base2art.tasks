﻿
namespace Base2art.Tasks
{
    using System;
    using System.Linq;
    using System.Threading.Tasks;

    public class ExceptionalFuncTaskThenner
    {
        private readonly Func<Task> task;
        private Func<Exception, Task> exceptionRunner;
        private Func<Task> successRunner;
        private Action<Exception> exceptionRunnerAction;
        private Action successRunnerAction;

        public ExceptionalFuncTaskThenner(Func<Task> task)
        {
            this.task = task;
        }

        protected Func<Task> Task
        {
            get
            {
                return task;
            }
        }
        
        public ExceptionalFuncTaskThenner Exception(Func<Exception, Task> exceptionRunner)
        {
            this.exceptionRunner = exceptionRunner;
            return this;
        }
        
        public ExceptionalFuncTaskThenner Success(Func<Task> successRunner)
        {
            this.successRunner = successRunner;
            return this;
        }
        
        public ExceptionalFuncTaskThenner Exception(Action<Exception> exceptionRunner)
        {
            this.exceptionRunnerAction = exceptionRunner;
            return this;
        }
        
        public ExceptionalFuncTaskThenner Success(Action successRunner)
        {
            this.successRunnerAction = successRunner;
            return this;
        }

        public Task Execute()
        {
            return this.Retry(0, 0);
        }

        public Task ExecuteWithRetry(int times)
        {
            return this.Retry(0, times);
        }

        private async Task Retry(int current, int times)
        {
            if (current == times)
            {
                Exception exi = null;
                try
                {
                    await this.Task();
                }
                catch (Exception ex)
                {
                    exi = ex;
                }
                
                if (exi != null)
                {
                    if (this.exceptionRunner != null)
                    {
                        await this.exceptionRunner(exi);
                    }
                    
                    if (this.exceptionRunnerAction != null)
                    {
                        this.exceptionRunnerAction(exi);
                    }
                    
                    throw exi;
                }
                
                if (this.successRunner != null)
                {
                    await this.successRunner();
                }
                
                if (this.successRunnerAction != null)
                {
                    this.successRunnerAction();
                }
            }
            else
            {
                try
                {
                    await this.Task();
                    
                    if (this.successRunner != null)
                    {
                        await this.successRunner();
                    }
                    
                    if (this.successRunnerAction != null)
                    {
                        this.successRunnerAction();
                    }
                    
                    return;
                }
                catch (Exception)
                {
                }

                await this.Retry(current + 1, times);
            }
        }
    }
}


