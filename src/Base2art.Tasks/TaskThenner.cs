﻿
namespace Base2art.Tasks
{
    using System;
    using System.Linq;
    using System.Threading.Tasks;

    public class TaskThenner
    {
        private readonly Task task;

        public TaskThenner(Task task)
        {
            this.task = task;
        }

        protected Task Task
        {
            get { return task; }
        }

        public virtual async Task<TResult> Return<TResult>(TResult result)
        {
            await this.Task;
            return result;
        }

        public virtual async Task<TResult> Return<TResult>(Func<TResult> resultProducer)
        {
            await this.Task;
            return resultProducer();
        }

        public virtual async Task<TResult> Return<TResult>(Func<Task<TResult>> resultProducer)
        {
            await this.Task;
            return await resultProducer();
        }

        public virtual async Task Execute(Func<Task> resultProducer)
        {
            await this.Task;
            await resultProducer();
        }

        public virtual async Task Execute(Action resultProducer)
        {
            await this.Task;
            resultProducer();
        }
    }
}




